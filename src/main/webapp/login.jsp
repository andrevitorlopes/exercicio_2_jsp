<%-- 
    Document   : login
    Created on : 28/09/2016, 14:46:17
    Author     : andre
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean class="pratica.jsp.loginBean" id="info" scope="session"/>
<jsp:setProperty name="info" property="*" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Login</title>
    </head>
    <body>
        <form method="post" action="login.jsp">
            Código: <input type='text' name='nome'/><br/>
            Nome: <input type='text' name='login'/><br/>
            Senha: <input type='password' name='senha'/><br/>
            Perfil: <select name="perfil">
                <option value="1">Cliente</option>
                <option value="2">Gerente</option>
                <option value="3">Administrador</option>
            </select>
            <input type="submit" value="Enviar"/>
        </form>

        <%if (request.getMethod().equalsIgnoreCase("POST")) {%>
        <c:choose>
            <c:when test="${info.login.equals(info.senha)}">
                <div style="color: blue">${info.perfilNome}, login bem sucedido, para ${info.login}</div>
            </c:when>
            <c:otherwise>
                <div style="color: red; font-style: italic">Acesso negado</div>
            </c:otherwise>
        </c:choose>
        <%}%>
    </body>
</html>

