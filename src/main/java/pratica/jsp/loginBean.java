/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pratica.jsp;

/**
 *
 * @author andre
 */
public class loginBean {
    private String login = "", nome = "", perfil = "", senha = "";
    private String perfilNome;
    
    public loginBean(){}

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public String getPerfilNome(){
        return this.perfil.equals("1") ? "Cliente" : perfil.equals("2") ? "Gerente" : "Administrador";
    }
    
    public String getLogin() {
        return login;
    }
    
    public void setLogin(String login) {
        this.login = login;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
    
    
}
